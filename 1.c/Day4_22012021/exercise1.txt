Viết chương trình dạng Menu chọn thực hiện các công việc sau (lưu ý mỗi công việc được thực hiện trên một hàm):

1. Nhập một số nguyên N (0<N<50).

2. Nhập một mảng gồm N số thực.

3. Tìm số lớn nhất trong mảng.

4. Tim số nhỏ nhất trong mảng.

5. Tìm số dương chan lớn nhất trong mảng.

6. Tìm số âm lẻ nhỏ nhất trong mảng.

7. Tim các số chính phương trong mảng

8. Tính tổng mảng.

9. Tỉnh trung binh cộng các phân tử mảng.

10. Tìm những phân tử lớn hơn trưng bình cộng.